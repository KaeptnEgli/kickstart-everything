#!/usr/bin/env bash

set -e

ERROR_CODE_NO_PURPOSE=1

if [ -z "${1}" ]; then
    echo "FATAL: No Purpose given for VM name. Exit Code ${ERROR_CODE_NO_PURPOSE}"
    exit 1
fi 

name="--name centos-stream-9-${1}"
memory="--memory 2048"
cpus="--cpu=host --vcpus=2"
disk="--disk size=40"
location="--location ../iso/CentOS-Stream-9-latest-x86_64-dvd1.iso"
boot="--boot uefi"
variant="--os-variant=centos-stream9"
graphics="--graphics spice"
network="--network bridge=virbr0"
console="--console pty,target_type=serial"
initrd="--initrd-inject ./ks.cfg"
extraargs="--extra-args=\"inst.ks=file:/ks.cfg\""
autostart="--autostart"

virt-install \
$name \
$memory \
$cpus \
$disk \
$boot \
$location \
$variant \
$graphics \
$network \
$console \
$initrd \
$extraargs \
$autostart
