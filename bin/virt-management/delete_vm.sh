#!/usr/bin/env bash

set -o errexit

if [ "${#}" -lt 1 ] || [ "${#}" -gt 2 ]
then
    echo 'Usage: ./delete_vm.sh ${DOMAIN} ${NVRAM}'
    exit 1
fi

DOMAIN=${1}
NVRAM=${2}

virsh shutdown --domain ${DOMAIN}
virsh destroy --domain ${DOMAIN}

if [ "${NVRAM}" = "false" ] || [ -z "${NVRAM}" ]
then
    virsh undefine --domain ${DOMAIN} 
else
    virsh undefine --domain ${DOMAIN} --nvram
fi
