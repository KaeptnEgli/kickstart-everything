#/usr/bin/env bash

# variables:
# ${1} name of the vm

set -eou pipefail

virsh domiflist ${1}

arp -e
