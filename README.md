# Kickstart Everything

This Repo attempts to showcase different ways how you can use a Kickastart. Git pull the repo and follow the code examples below according to your use case.

**Default Password for all Users: sFa_23FF$!**

## Kickstart a QEMU/KVM virtual machine with virt-install

**Note:** You need to download the ISO images on your own. Those are not contained in this repository. Create a ```iso/``` directory from the repository root: ```mkdir iso/``` and place all your ISO's in there. This way the paths should match with the ones in the scripts. Anyway make sure that you double check the paths and the naming of the ISO images, so that they match with the ones in the shell scripts. For each distro there is a link mentioned where you can download the ISO image.

**Example:**
```
usekickstart@comkickstart:~/path/to/this/repository$ ls -alh iso/
total 14G
drwxrwxr-x 2 usekickstart usekickstart 4.0K Jun 17 15:25 .
drwxrwxr-x 8 usekickstart usekickstart 4.0K Jun 17 16:18 ..
-rw-rw-r-- 1 usekickstart usekickstart 8.1G Mai 24 12:46 CentOS-Stream-9-latest-x86_64-dvd1.iso
-rw-rw-r-- 1 usekickstart usekickstart 2.6G Jun 17 15:25 Fedora-Silverblue-ostree-x86_64-36-1.5-custom.iso
-rw-rw-r-- 1 usekickstart usekickstart 2.6G Jun 10 14:44 Fedora-Silverblue-ostree-x86_64-36-1.5.iso
```


### Centos-Stream-9

**Image Download:**

https://centos.org/centos-stream/

**Usage:**

```
$ cd centos-stream
$ ./centos-stream-9.sh ansible-awx
```

**Check Result:**

```
$ virsh list
 Id   Name                            State
-----------------------------------------------
 1   centos-stream-9-ansible-awx     running
```

### Fedora-Silverblue-36

**Image Download:**

https://silverblue.fedoraproject.org/download

**Usage:**

```
$ cd fedora-silverblue
$ ./fedora-silverblue-36.sh workstation-1
```

**Check Result:**

```
$ virsh list
 Id   Name                                 State
---------------------------------------------------
 1   fedora-silverblue-36-workstation-1   running
```

## Pack a Kickstart file into an ISO image.

In this aection you will see how you can pack a Kickstart directly in your ISO image. This is usfull if you want to create a bootstick that can Kickstart install your OS. The new ISO image will be created with the ```build_ks_iso.sh```script, which you can find in the ```bin/``` directory of this repository. 

**Prerequesites:** 

- A ISO image of any distribution you would like to kickstart.
- A custom Kickstart file named ks.cfg.
- You need to install ```implantisomd5```on your system.

**Usage:**

```$ ./build_ks_iso.sh ${VOLUME_LABEL} ${KS_PATH} ${ISO_INPUT_PATH} ${ISO_OUTPUT_PATH}```

**Example:**

Here you can see an example how to pack your custom Kickstart file into a Fedora Silverblue ISO. Make sure you adjust the paths and names according to your setup.

```
$ cd bin/kickstart-iso
$ ./build_ks_iso.sh Fedora-Silverblue-ostree-x86_64 ../fedora-silverblue/ks.cfg ../iso/Fedora-Silverblue-ostree-x86_64-36-1.5.iso ../iso/Fedora-Silverblue-ostree-x86_64-36-1.5-custom.iso
```

## Virtual Machine Management

In ```bin/virt-management```you can find a few scritps that concatenat common virtlib use cases that consist of multiple commands.

### delete_vm.sh

**Note:** Virtual Machines that use NVRAM need to set tha ${NVRAM} flag to true in order to delete it properly.

**Usage:** 
```
$ ./delete_vm.sh ${DOMAIN} ${NVRAM}
```

**Example:**
```
$ ./delete_vm.sh centos-stream-9-pxe-server true
```

----

If you having any troubles, you can contact me at the following e-mail address: nils.kreienbuehl(ät)protonmail.com


